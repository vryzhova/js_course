const createDebounceFunction = (callbackFunction, delay ) =>  {
    if(typeof callbackFunction !== 'function') throw new Error('callback должна быть функцией');
    if(typeof delay !== 'number' ) throw new Error('задержка это числовое значение');
    let timeout;
    return function(){
        clearTimeout(timeout);
        timeout = setTimeout(()=>{
            callbackFunction.apply(this, arguments);
        },delay)
    }
}