Array.prototype.myFilter = function(callbackFunction, contextObj) {
    if(typeof callbackFunction !== 'function') throw new Error('callback должна быть функцией');
    let result = [];
    let context = this;
    if(contextObj !== undefined) {
        context = contextObj;
    }

    let arrayLength = this.length;
    for( let i = 0; i < arrayLength; i++) {
        if(callbackFunction.call(context,this[i], i, this)){
            result.push(this[i]);
        }
    }
    return result;

}
