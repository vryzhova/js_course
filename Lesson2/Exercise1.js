
const makeObjectDeepCopy = (obj) => {
    if(obj === null) return null;
    const deepCopy = Array.isArray(obj) ? []:{};
    for(const key in obj){
        if(typeof key === 'object') {
            deepCopy[key] = makeObjectDeepCopy(obj[key]);
            continue;
        }
        deepCopy[key] = obj[key];
        
    }
    return deepCopy
}
makeObjectDeepCopy(firstobj)


