const arr = [1,2,3,5];
const a = 2;
const b = 5;

const selectFromInterval = (numbers,firstIntervalValue, lastIntervalValue) => {
    const isArray = Array.isArray(numbers);
    const isNumberArray = numbers.every(elem => typeof elem === 'number') &&  numbers.length !== 0 ;
    const isCorrectInputValues = !isNaN(firstIntervalValue) && !isNaN(lastIntervalValue);
    const inputConditions = !isArray || !isNumberArray || !isCorrectInputValues ;
    if(inputConditions){
        throw new Error('Неккоректный ввод');
    }
    let newArray;
    if(firstIntervalValue <= lastIntervalValue ) {
        newArray =  numbers.filter(elem => (elem >=firstIntervalValue && elem <= lastIntervalValue ));
        console.log(newArray)
    }else if(firstIntervalValue > lastIntervalValue){
        newArray =  numbers.filter(elem => (elem <=firstIntervalValue && elem >= lastIntervalValue));
        console.log(newArray)
    }else {
        throw new Error('Ошибка :(');
    }


}

selectFromInterval(arr,a,b);