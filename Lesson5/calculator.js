let firstNumber = '';
let secondNumber = '';
let operand = '';
let finish = false;

const digit = ['0','1','2','3','4','5','6','7','8','9', '.'];
const operands = ['-', '+', 'x','/']

const out = document.getElementById('result');

const clearAll = () => {
    firstNumber = '';
    secondNumber = '';
    operand = '';
    finish = false;
    out.textContent = 0;
}

const calcBtn = document.querySelector('.calc-buttons');
const isSecondOperandEmpty = firstNumber !== '' && secondNumber ==='';
const isSecondOperandNotEmpty = firstNumber !== '' && secondNumber !=='';


calcBtn.addEventListener('click',(event) => {
    if(!event.target.classList.contains('num_btn')) return;
    if(event.target.classList.contains('clearAll')) return;

    out.textContent = '';
    const key = event.target.textContent;

    if (digit.includes(key)) {
        if(secondNumber === '' && operand === ''){
            firstNumber+=key;
            out.textContent = firstNumber;
            

        } else if (firstNumber !== '' && secondNumber !==''  && finish) {

            secondNumber = key;
            finish = false;
            out.textContent = secondNumber;

        }else {

            secondNumber += key;
            out.textContent = secondNumber;
            
        }
    }

    if (operands.includes(key)) {
        operand = key;
        out.textContent = operand;
        console.log(firstNumber,secondNumber,operand);
    }
    if (key === '+/-') {
        if(firstNumber !== '' && secondNumber ==='') {
            firstNumber = -firstNumber;
            out.textContent = firstNumber;
        }else if(firstNumber !== '' && secondNumber !=='') {
            secondNumber = -secondNumber;
            out.textContent = secondNumber;
        }
    }

    // if(key === 'del') {
    //     if(secondNumber === '' && operand === '') {
    //         firstNumber = '';
    //         out.textContent = firstNumber;
    //         console.log(firstNumber,secondNumber,operand);
            
    //     }else if(firstNumber !== '' && secondNumber !== '') {
    //         secondNumber = '';
    //         console.log(firstNumber,secondNumber,operand);
            
    //     }else {
    //         firstNumber = '',
    //         secondNumber = ''
    //     }
    // }

    if(key === '=') {
        if( secondNumber === '') secondNumber = firstNumber;
        switch (operand) {
            case '+':
                firstNumber = (Number(firstNumber) + Number(secondNumber)).toFixed(8);
                break;
            case '-':
                    firstNumber = (Number(firstNumber) - Number(secondNumber)).toFixed(9);
                break; 
            case 'x':
                    firstNumber = ((Number(firstNumber) * Number(secondNumber))).toFixed(9);
                break;   
            case '/':
                if(secondNumber === '0') {
                    out.textContent = 'Ошибка';
                    firstNumber = '';
                    secondNumber = '';
                    operand = '';
                    return;
                }else {
                    firstNumber = (Number(firstNumber) / Number(secondNumber)).toFixed(9);
                }
                break;       
        }
        finish = true;
        out.textContent = firstNumber;
    }
    
}) 

