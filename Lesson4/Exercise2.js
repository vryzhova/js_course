const isValidNumber = (number) =>  Number.isFinite(number)
const isValidArguments = (...args) => args.every(isValidNumber);

class Calculator  {
    constructor(num1,num2) {
        if(!isValidArguments(num1,num2)) throw new Error('Вводные данные некорректны')
        this.num1 = num1;
        this.num2 = num2;
        this.logSum = this.logSum.bind(this);
        this.logMul = this.logMul.bind(this);
        this.logSub = this.logSub.bind(this);
        this.logDiv = this.logDiv.bind(this);

    }

    setX(number) {
        if(!isValidNumber(number)) throw new Error('Вводные данные некорректны')
        this.num1=number;
        
    }
    setY(number) {
        if(!isValidNumber(number)) throw new Error('Вводные данные некорректны')
        this.num2=number;
    }

    logSum() {
        console.log( this.num1 + this.num2)
    }
    logMul() {
        console.log( this.num1*this.num2)
    }
    logSub()  {
        console.log( this.num1 - this.num2)
    }
    logDiv() {
        if(this.num2 === 0) throw new Error('На ноль делить нельзя') 
        console.log(this.num1/this.num2)
    }
} 

const calculator = new Calculator(12, 3);
calculator.logSum();
calculator.logDiv(); 
calculator.setX(15);
calculator.logDiv(); // 5
const logCalculatorDiv = calculator.logDiv; 
logCalculatorDiv(); // 5 
calculator.setY(444n); // Ошибка!
