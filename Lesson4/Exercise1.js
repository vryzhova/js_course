const concatStrings = (str, separator = '') => {
    const checkValidStr = (checkString) => {return typeof checkString === 'string'};
    separator = checkValidStr(separator)? separator : '';
    let finalString = str;
    return function (newStr) {
        if (!checkValidStr(newStr)) {
            return finalString;
        }
        
        finalString += `${separator}${newStr}`;
        return concatStrings.call(this, finalString, separator);
    } ;

}
